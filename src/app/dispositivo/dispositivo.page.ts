import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dispositivo } from '../model/Dispositivo';
import { DispositivoService } from '../services/dispositivo.service';

@Component({
  selector: 'app-dispositivo',
  templateUrl: './dispositivo.page.html',
  styleUrls: ['./dispositivo.page.scss'],
})
export class DispositivoPage implements OnInit {

  private _dispositivo: Dispositivo;
  public get dispositivo(): Dispositivo {
    return this._dispositivo;
  }
  public set dispositivo(value: Dispositivo) {
    this._dispositivo = value;
  }

  constructor(private router: ActivatedRoute, private dServ: DispositivoService) { }

  ngOnInit() {
    let receivedId: number = Number(this.router.snapshot.paramMap.get('id'))
    this.dispositivo = this.dServ.getDispositivoById(receivedId)
  }
}
